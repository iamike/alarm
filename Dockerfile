FROM node:latest
COPY . /var/www/web



# Copy our code from the current folder to /html inside the container
#VOLUME ['/']


WORKDIR /var/www/web

#RUN yarn global add @quasar/cli

RUN yarn install

ENTRYPOINT ["bash","docker-entrypoint.sh"]

#CMD tail -f /dev/null

#HEALTHCHECK CMD curl --fail http://localhost:8080/ || exit 1


EXPOSE 8080
